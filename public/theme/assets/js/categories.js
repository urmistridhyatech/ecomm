$(document).ready(function()
{

	 $.ajaxSetup({
        headers : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	
fetchRecord();

function fetchRecord()
	{
    $("#tbody").empty();
		
	    $.ajax(
			{				
				type     : 'GET',
				url		:'/getCategories',
				dataType : 'json',
				success  : function(data)
					{
						
						$.each(data, function(index, value)
		 				{
		 		          $("#tbody").append("<tr><td>"+value['name']+"</td><td><a class='btn btn-danger edt' data-toggle='modal' data-target='#myModal2' id='"+value['id']+"'>Edit</a></td><td><a class='btn btn-info del' id='"+value['id']+"'>Delete</a></td></tr>");							 			
						});
					}
			}
		 );
	}




	$(document).on('click', '.del', function () 
{
	var task_id=$(this).attr('id');	
	var jd={'id' : task_id};
	$.ajax({
                type:'GET',
                url: "deleteTask",
                data: jd,
                success : function(data)
					{
						if(data=="ok")
						{
								fetchRecord();
						}
						else
						{
							alert("fail to delete");									
						}
                	}
            });	

});
$(document).on('click', '.edt', function () 
{
	
	$(".er").text("");

		var id=$(this).attr('id');	
		
		var currentRow=$(this).closest("tr"); 
         
         
         var name=currentRow.find("td:eq(0)").text(); 
         $("#name").val(name);
         $("#id").val(id);

			

      
});


$("#categoryForm").on('submit',function(e)
{

	e.preventDefault();
	$.ajax({

		type:"GET",
		url:"createCategory",
		data:$("#categoryForm").serialize(),
		success:function(result)
		{
			
			fetchRecord();
			$("#myModal").modal('hide');
			// document.getElementById("mybutton").click();
		}
	});
});


$("#editCategoryForm").on('submit',function(e)
{

	e.preventDefault();
	$.ajax({

		type:"GET",
		url:"updateCategory",
		data:$("#editCategoryForm").serialize(),
		success:function(result)
		{
			
			fetchRecord();
			$("#myModal2").modal('hide');
			
			
		}
	});
});


});