<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Redirect,Response;

class CategoryController extends Controller
{
    public function categories()
    {
    	
        
return view('admin.categories');

    }
    public function getCategories()
    {
       
        $record = Category::get()->toArray();
        echo json_encode($record);

    }
    public function createCategory(Request $request)
    {
    	$category=new Category;
    	
        
        $category->name = $request->name;
        
         $category->save();
    	echo json_encode(["ans"=>	"1"]);
    }
    public function updateCategory(Request $request)
    {
    	 $task= Category::where('id',$request->id)->update(["name"=>$request->name]);
       ;
        echo json_encode(["ans"=>   "1"]);
    }
}
